from os import listdir
import codecs
import time
import re

path_to_movie_list = "H:/Code/kpApi/moviesList.txt"

f = codecs.open(path_to_movie_list, 'r', 'utf_8_sig')
text = f.read()

all_lines = text.splitlines()

path_to_movie_posters = "H:/Code/kpApi/moviePosters/"

img_source = 'H:/moviesList/1/inst1'

page = 4
while page <= 4:
    list_dir = listdir(path_to_movie_posters + str(page))
    list_dir.sort(key=lambda f: int(re.sub('\D', '', f)))
    for file in list_dir:
        if file.endswith('-inst.jpg'):
            movie_id = file[:-9]
            for movie_line in all_lines:
                movie_id_from_file = movie_line.split(" | ")[0]
                movie_name_en_from_file = movie_line.split(" | ")[1]
                movie_name_rus_from_file = movie_line.split(" | ")[2]
                movie_date_from_file = movie_line.split(" | ")[3]
                movie_vote_from_file = movie_line.split(" | ")[4]
                movie_description_from_file = movie_line.split(" | ")[5]
                if movie_id_from_file == movie_id:
                    name_eng_tags = movie_name_en_from_file.strip(" ")
                    name_eng_tags = name_eng_tags.replace(" ", "")
                    name_rus_tags = movie_name_rus_from_file.strip(" ")
                    name_rus_tags = name_rus_tags.replace(" ", "")
                    not_recommended = ""
                    if(int(movie_vote_from_file) < 5):
                        not_recommended = "P.S. НЕ рекомендую к просмотру :) "
                    hashtags = f"#{name_eng_tags.lower()} " \
                               f"#{name_rus_tags.lower()[:-6]} " \
                               f"#movie " \
                               f"#movies " \
                               f"#film " \
                               f"#films " \
                               f"#goodmovie " \
                               f"#bestfilm " \
                               f"#tv " \
                               f"#hobby " \
                               f"#video " \
                               f"#entertainment " \
                               f"#кино " \
                               f"#фильм " \
                               f"#фильмы " \
                               f"#хобби " \
                               f"#развлечения " \
                               f"#видео"

                    spreader_text_rus = "спридеров"
                    spreader_text_eng = "spreaders"
                    if(int(movie_vote_from_file) == 1):
                        spreader_text_rus = "спридер"
                    if (int(movie_vote_from_file) >= 2 and int(movie_vote_from_file) <= 4):
                        spreader_text_rus = "спридера"
                    postString = f"{movie_name_rus_from_file} / " \
                                 f"{movie_name_en_from_file} " \
                                 f"[{movie_vote_from_file} {spreader_text_rus} из 10] " \
                                 f"[{movie_vote_from_file} out of 10 {spreader_text_eng}] " \
                                 f"{not_recommended}" \
                                 f"{movie_description_from_file} " \
                                 f"{hashtags}"
                    print(postString)
                    file_name = path_to_movie_posters + str(page) + f"/{page}.txt"
                    print(f"Appending to {file_name}")
                    with open(file_name, 'a', encoding='utf-8') as output_file:
                        output_file.write(postString + "\n")
    print(f"Page №{page} is done\n")
    page += 1



