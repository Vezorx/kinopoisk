import cv2
import os
from os import listdir

standardW = 500
standardH = 500

def make_square(image_in):
   size = image_in.shape[:2]
   max_dim = max(size)
   delta_w = max_dim - size[1]
   delta_h = max_dim - size[0]
   top, bottom = delta_h//2, delta_h-(delta_h//2)
   left, right = delta_w//2, delta_w-(delta_w//2)
   color = [0, 0, 0]
   image_out = cv2.copyMakeBorder(image_in, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)
   # image_out = cv2.copyMakeBorder(image_in, top, bottom, left, right, cv2.BORDER_REPLICATE, value=color)
   dim = (standardW, standardH)
   resized = cv2.resize(image_out, dim, interpolation=cv2.INTER_AREA)
   return resized

def create_dir(path):
   try:
      os.mkdir(path)
   except OSError:
      print("Creation of the directory %s failed" % path)
   else:
      print("Successfully created the directory %s " % path)

#FOR DOWNLOADING 1 MOVIE
source_images = 'H:/Code/kpApi/movies/'

page_source = source_images
for file in listdir(page_source):
   print(file)
   if file.endswith('png'):
      file_name = page_source + file
      print(f"Opening image {file_name}")
      image_inJR = cv2.imread(file_name)
      i = make_square(image_inJR)
      out_write_file_name = file_name[:-4] + "-inst.jpg"
      print(f"Saving new square image {out_write_file_name}")
      cv2.imwrite(f"{out_write_file_name}", i)

#FOR DOWNLOADING ALL MOVIEs
# source_images = 'H:/Code/kpApi/moviePosters/'
#
# page = 4
# while page <= 4:
#    page_source = source_images + str(page) + "/"
#    create_dir(page_source)
#
#    for file in listdir(page_source):
#       print(file)
#       if file.endswith('png'):
#          file_name = page_source + file
#          print(f"Opening image {file_name}")
#          image_inJR = cv2.imread(file_name)
#          i = make_square(image_inJR)
#          out_write_file_name = file_name[:-4] + "-inst.jpg"
#          print(f"Saving new square image {out_write_file_name}")
#          cv2.imwrite(f"{out_write_file_name}", i)
#    page += 1


